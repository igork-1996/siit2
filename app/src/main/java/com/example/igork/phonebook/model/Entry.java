package com.example.igork.phonebook.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Entry extends RealmObject {
    @PrimaryKey
    private int id;
    private String name;
    private String surname;
    private String adress;

    public Entry()
    {

    }

    public Entry(String name, String surname, String adress) {
        this.name = name;
        this.surname = surname;
        this.adress = adress;
        this.id= -1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
