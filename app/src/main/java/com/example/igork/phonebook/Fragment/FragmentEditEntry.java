package com.example.igork.phonebook.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.igork.phonebook.R;
import com.example.igork.phonebook.model.Entry;

import io.realm.Realm;


public class FragmentEditEntry extends Fragment {
    View view;
    Entry entry;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_edit_entry, container, false);
        Button buttonSave = (Button) view.findViewById(R.id.buttonSave);
        final EditText editTextName = (EditText) view.findViewById(R.id.editTextName);
        editTextName.setText(entry.getName());
        final EditText editTextSurname = (EditText) view.findViewById(R.id.editTextSurname);
        editTextSurname.setText(entry.getSurname());
        final EditText editTextAddress = (EditText) view.findViewById(R.id.editTextAddress);
        editTextAddress.setText(entry.getAdress());
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                entry.setName(editTextName.getText().toString());
                entry.setSurname(editTextSurname.getText().toString());
                entry.setAdress(editTextAddress.getText().toString());
                realm.commitTransaction();
                Toast.makeText(getActivity(),"Saved",Toast.LENGTH_LONG).show();
            }
        });
        return view;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }
}
