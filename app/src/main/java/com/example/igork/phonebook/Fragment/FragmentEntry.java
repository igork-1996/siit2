package com.example.igork.phonebook.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.igork.phonebook.R;
import com.example.igork.phonebook.model.Entry;


public class FragmentEntry extends Fragment {
    View view;
    Entry entry;
    FragmentEntryInterface fragmentEntryInterface;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_entry,container,false);
        TextView textViewName = (TextView) view.findViewById(R.id.textViewName);
        textViewName.setText(entry.getName());
        TextView textViewSurname = (TextView) view.findViewById(R.id.textViewSurname);
        textViewSurname.setText(entry.getSurname());
        TextView textViewAddress = (TextView) view.findViewById(R.id.textViewAddress);
        textViewAddress.setText(entry.getAdress());
        Button buttonEditEntry = (Button) view.findViewById(R.id.buttonEditEntry);
        buttonEditEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentEntryInterface.editEntryButton(entry);
            }
        });
        return view;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }

    public void setFragmentEntryInterface(FragmentEntryInterface fragmentEntryInterface) {
        this.fragmentEntryInterface = fragmentEntryInterface;
    }

    public interface FragmentEntryInterface
    {
        void editEntryButton(Entry entry);
    }
}
