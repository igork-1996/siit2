package com.example.igork.phonebook.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.igork.phonebook.Activity.MainActivity;
import com.example.igork.phonebook.EntriesAdapter;
import com.example.igork.phonebook.R;
import com.example.igork.phonebook.model.Entry;

import java.util.ArrayList;



public class FragmentListEntry extends Fragment {

    View view;
    ArrayList<Entry> entries;
    FragmentListEntryInterface fragmentListEntryInterface;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_list_entry,container,false);

        Button buttonAdd = (Button) view.findViewById(R.id.button_add);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentListEntryInterface.onAddButtonClick();
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        entries = ((MainActivity)getActivity()).getPhoneBook();
        ListView listViewEntries = (ListView) view.findViewById(R.id.list_entries);
        listViewEntries.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                fragmentListEntryInterface.onEntryClick(entries.get(position));
            }
        });
        EntriesAdapter entriesAdapter = new EntriesAdapter(entries,getActivity());
        listViewEntries.setAdapter(entriesAdapter);
        super.onResume();
    }

    public void setPhoneBook(ArrayList<Entry> entries)
    {
        this.entries = entries;
    }

    public void setFragmentListEntryInterface(FragmentListEntryInterface fragmentListEntryInterface) {
        this.fragmentListEntryInterface = fragmentListEntryInterface;
    }

    public interface FragmentListEntryInterface
    {
        void onEntryClick(Entry entry);
        void onAddButtonClick();
    }
}
