package com.example.igork.phonebook;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.igork.phonebook.model.Entry;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by igork on 23.11.2016.
 */

public class EntriesAdapter extends BaseAdapter {
    private ArrayList<Entry> entries;
    private Context context;
    private LayoutInflater inflater;
    public EntriesAdapter(ArrayList<Entry> entries, Context context) {
        this.entries = entries;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return entries.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(R.layout.item_entry,parent,false);
        TextView name= (TextView) view.findViewById(R.id.textViewName);
        TextView surname= (TextView) view.findViewById(R.id.textViewSurname);
        name.setText(entries.get(position).getName());
        surname.setText(entries.get(position).getSurname());
        return view;
    }
}
