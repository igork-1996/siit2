package com.example.igork.phonebook.Activity;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.example.igork.phonebook.Fragment.FragmentEditEntry;
import com.example.igork.phonebook.Fragment.FragmentEntry;
import com.example.igork.phonebook.Fragment.FragmentListEntry;
import com.example.igork.phonebook.R;
import com.example.igork.phonebook.model.Entry;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity
        implements FragmentListEntry.FragmentListEntryInterface, FragmentEntry.FragmentEntryInterface {
    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Realm.init(getApplicationContext());
        realm = Realm.getDefaultInstance();

        ArrayList<Entry> entries = getPhoneBook();
        //copyOrUpdateEntry(temp);
        FragmentListEntry fragmentListEntry = new FragmentListEntry();
        fragmentListEntry.setPhoneBook(entries);
        fragmentListEntry.setFragmentListEntryInterface(this);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragmentListEntry);
        fragmentTransaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        } else
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public ArrayList<Entry> getPhoneBook() {

        ArrayList<Entry> entries = new ArrayList<>();
        try {
            realm.beginTransaction();
            RealmResults<Entry> result = realm.where(Entry.class).findAll();
            realm.commitTransaction();
            for (Entry entry : result) {
                entries.add(entry);
            }
        } catch (Exception e) {
            realm.cancelTransaction();
        }
        return entries;
    }

    private void addToRealm(Entry entry) {
        try {
            realm.beginTransaction();
            if (entry.getId() == -1) {
                int newId = 0;
                Number number = realm.where(Entry.class).max("id");
                if (number != null) {
                    newId = number.intValue() + 1;
                }
                entry.setId(newId);
            }
            realm.copyToRealmOrUpdate(entry);
            realm.commitTransaction();
        } catch (Exception e) {
            realm.cancelTransaction();
        }
    }

    @Override
    public void onEntryClick(Entry entry) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        FragmentEntry fragmentEntry = new FragmentEntry();
        fragmentEntry.setEntry(entry);
        fragmentEntry.setFragmentEntryInterface(this);
        fragmentTransaction.replace(R.id.container, fragmentEntry);
        fragmentTransaction.addToBackStack("fragment_entry");
        fragmentTransaction.commit();
    }

    @Override
    public void onAddButtonClick() {
        Entry entry = getPhoneBook().get(getPhoneBook().size()-1);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        FragmentEditEntry fragmentEditEntry = new FragmentEditEntry();
        fragmentEditEntry.setEntry(entry);
        fragmentTransaction.replace(R.id.container, fragmentEditEntry);
        fragmentTransaction.addToBackStack("fragment_edit_entry");
        fragmentTransaction.commit();
    }

    @Override
    public void editEntryButton(Entry entry) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        FragmentEditEntry fragmentEditEntry = new FragmentEditEntry();
        fragmentEditEntry.setEntry(entry);
        fragmentTransaction.replace(R.id.container, fragmentEditEntry);
        fragmentTransaction.addToBackStack("fragment_edit_entry");
        fragmentTransaction.commit();
    }
}
